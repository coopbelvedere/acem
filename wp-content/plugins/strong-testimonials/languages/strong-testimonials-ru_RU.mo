��    v      �  �   |      �	      �	     
      
     4
     @
     F
     J
     [
     j
  
   q
     |
     �
     �
     �
     �
     �
     �
     �
     �
                    /     4     H     [  !   b     �     �     �     �     �  8   �  h   �     ]     l     s  	   �     �     �     �     �     �     �     �     �     �     �  
                  !     -  	   3     =  /   J     z     �     �     �  	   �     �     �  	   �  
   �     �  	   �                    /     ?     N     Z     n     �     �  i   �  h     /   q     �     �  	   �     �  �   �  �   �     m  5   z  Q   �               /     J     f     ~  *   �  "   �  ;   �  r        �  *   �     �  +   �  !     
   $     /     7     K     a     m     {     �     �  #   �  #   �       )     �  ;  4     $   :  &   _     �  
   �     �     �     �  
   �     �     �  $     %   6  &   \  !   �  7   �  !   �     �  0     !   J     l     �     �  /   �  ?   �       1   &  %   X     ~     �     �     �  J   �  �     )   �     �     �          	     #     6     9     W     m      t  2   �     �     �  +   �          %     8  
   L     W     o  q   �     �  0        G     e          �  *   �     �     �  $   �           "      '   M   C      �      �   
   �   !   �   !   �      !  
   6!  �   A!  �   �!  `   �"  ,   4#  :   a#     �#     �#  �  �#  q  X%  #   �&  y   �&  �   h'  !   #(  %   E(  6   k(  4   �(  >   �(     )  _   %)  :   �)  h   �)  �   )*     �*  B   �*  &   :+  6   a+  ;   �+     �+     �+      �+  )    ,     J,     b,  (   ~,  $   �,  .   �,     �,  
   -     -  _   ,-     >   ;   _      0       h   F                 E       Y   
   6   /   I              ?      v   H   "   7   A      D              a               S   :   p      s   n           8   M      4   B   ,   K   c   (          W   q       U   @   =   ]   [   f   Z   P   G   -          R             !   5       t   <             l                                 )   o           $   +   X   9   e       &   .   u      i      #                  L      `   C       1      k       J   V   %      2   r   b   '   N   j       T   \   *                    m          Q   3       ^           d   	      g   O    A headline for your testimonial. Add New Field Add New Testimonial Admin Table After All All Testimonials All categories Before Categories Category Changes undone. Character limit Client Info Client Section Client section shortcodes: Company Name Company Website Custom Field Group Custom Fields Cycle Cycle Shortcode Date Default Field Group Defaults restored. Delete Does your company have a website? Edit Testimonial Email Entire content Examples Excerpt Excerpts are hand-crafted summaries of your testimonial. Excerpts are hand-crafted summaries of your testimonial. You may need to enable them in the post editor. Featured Image Fields Fields saved. Full Name Getting Started Heading ID Load stylesheets New Testimonial None Nothing Found Nothing found in Trash Notification Pages Pagination Photo Placeholder Post Fields Posts Read More Recommended. Remember to save changes before switching tabs. Required Restore Default Restore Defaults Search Testimonials Selection Settings Settings saved. Shortcode Shortcodes Show All Slideshow Slug Spam Control Strong Testimonials Guide Submission Form Template files Testimonial Testimonial Content Testimonial Settings Testimonial Title The Form These are the fields used on your testimonial submission form. You can change these in the Fields editor. These methods are both time-tested and widely used. They can be used simultaneously for more protection. These shortcodes only work here, not on a page. This appears on the form. This field is required. Thumbnail Title Traps spambots by adding an extra empty field that is invisible to humans but not to spambots which tend to fill in every field they find in the form code. Empty field = human. Not empty = spambot. Traps spambots by using JavaScript to add a new field as soon as the form is submitted. Since spambots cannot run JavaScript, the new field never gets added. New field = human. Missing = spambot. Undo Changes Use only lowercase letters, numbers, and underscores. Use these shortcodes to select which client fields appear below each testimonial. View Testimonial What do you think about us? What is your company name? What is your email address? What is your full name? Widget Will break on a space and add an ellipsis. Would you like to include a photo? You do not have sufficient permissions to access this page. You may need to enable them in the post editor like in this <a id="toggle-screen-options" href="#">screenshot</a>. Your fields: a form for visitors to submit testimonials a single random testimonial a single testimonial from a single category and add them to a single category categories default from all categories if included in Fields no settings not installed one specific testimonial or a single category or multiple categories post type general nameTestimonials post type singular nameTestimonial required will break on a space and add an ellipsis Project-Id-Version: Strong Testimonials v1.11.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-10-13 12:10-0400
PO-Revision-Date: 2015-10-13 12:10-0400
Last-Translator: Chris Dillon <chris@wpmission.com>
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 Заголовок для вашего отзыва. Добавить Новое Поле Добавить Новый Отзыв Админ Таблица После Все Все Отзывы Все категории Перед Категории Категория Изменения отменены. Показывать символов Информация о Клиенте Клиентский Раздел Шорткоды клиентского раздела: Название Компании Сайт Компании Группа Произвольных Полей Произвольные Поля Цикл Отзывов Шорткод Цикла Дата Группа Полей По Умолчанию Исходные установки восстановлены. Удалить У вашей компании есть сайт? Редактировать Отзыв Email Весь контент Примеры Цитата Цитаты - это ручное резюме вашего отзыва. Цитаты - это ручное резюме вашего отзыва. Возможно, вам придется включить их в редакторе постов. Избранное Изображение Поля Поля сохранены. Имя Начало Работы Заголовок ID Загрузка стилей Новый Отзыв Нет Ничего не найдено Ничего не найдено в корзине Уведомление Страницы Постраничная навигация Фото Подсказка Поля Поста Посты Читать далее Рекомендуется. Не забудьте сохранить изменения перед переключением вкладок. Обязательное Восстановить По Умолчанию Сброс Установок Поиск Отзывов Выборка Настройки Настройки сохраняются. Шорткод Шорткоды Показать Все Отзывы Слайдшоу Slug Контроль Спама Сильные Отзывы - Руководство пользователя Формы Отправки Файлы шаблона Отзыв Содержание Отзыва Настройки Отзывов Заголовок отзыва Форма Эти поля используются в вашей форме отправки отзывов. Вы можете изменить их в Редакторе полей. Эти методы проверенные временем и широко используемые. Они могут быть использованы одновременно для дополнительной защиты. Эти шорткоды только работать здесь, а не на странице. Это отобразится в форме. Это поле является обязательным. Миниатюра Заголовок Ловушки спам-ботов добавляют дополнительное пустое поле, которое невидимо для человека, но не для спам-ботов, которые, как правило, заполняют все поля, которые они находят в виде кода. Пустое поле = человека. Не пустое = spambot. Ловушки спам-ботов используют JavaScript, чтобы добавить новое поле, как только форма отправлена. Так как спам-боты не могут выполнять JavaScript, новое поле не добавляется. Новое поле = человек. Отсутствует = spambot. Отменить Изменения Используйте только строчные буквы, цифры и символы подчеркивания. Используйте эти шорткоды для выбора клиентских полей, которые будут отображаются под каждым отзывом. Просмотреть Отзыв Что Вы о нас думаете? Как называется ваша компания? Ваш адрес электронной почты? Как Вас зовут? (имя, или полное ФИО) Виджета Будет обрываться на пробеле и добавится многоточие. Хотели бы Вы добавить ваше фото? Вы не имеете достаточно прав для доступа к этой странице. Вам может потребоваться включить их в редакторе постов, как на этом <a id="toggle-screen-options" href="#">скриншоте</a>. Ваши поля: форма для сбора отзывов посетителей один случайный отзыв один отзыв из одной категории и добавления их в одну категорию категории по умолчанию из всех категорий если включено в ''Поля'' нет настроек не установлено один конкретный отзыв или одной категории или нескольких категорий Отзывы Отзыв обязательное будет обрываться на пробеле и добавится многоточие. 