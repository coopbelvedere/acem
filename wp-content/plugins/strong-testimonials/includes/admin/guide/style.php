<!-- STYLE -->
<?php /* translators: In the [strong] shortcode instructions. */ ?>
<tr class="section">
	<th colspan="4">
		<h4><?php _e( 'Style', 'strong-testimonials' ); ?></h4>
	</th>
</tr>
<tr class="sub">
	<th><?php _e( 'attribute', 'strong-testimonials' ); ?></th>
	<th><?php _e( 'purpose', 'strong-testimonials' ); ?></th>
	<th><?php _e( 'default', 'strong-testimonials' ); ?></th>
	<th><?php _e( 'accepted values', 'strong-testimonials' ); ?></th>
</tr>
<tr class="att">
	<td>template</td>
	<td><?php _e( 'to use a custom template', 'strong-testimonials' ); ?><br>
	</td>
	<td></td>
	<td></td>
</tr>
<tr class="att">
	<td>class</td>
	<td>
		<?php _e( 'to add a CSS class', 'strong-testimonials' ); ?>
	</td>
	<td></td>
	<td><?php _e( 'a list of class names, separated by commas', 'strong-testimonials' ); ?></td>
</tr>

<tr class="example">
	<td colspan="4" class="has-inner">
		<table>
			<tr class="sub">
				<th><?php _e( 'examples', 'strong-testimonials' ); ?></th>
			</tr>
			<tr>
				<td>[strong template="custom" &hellip;]</td>
			</tr>
			<tr>
				<td>[strong class="round,blue" &hellip;]</td>
			</tr>
		</table>
	</td>
</tr>
