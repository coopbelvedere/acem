<div class="guide-content views">

	<?php do_action( 'wpmtst_guide_before_content' ); ?>

	<h3><?php _e( 'Views', 'strong-testimonials' ); ?></h3>
	
	<p><?php _e( 'A View can display your testimonials, create a slideshow, or show the front-end form. Views are much easier to learn and adjust as they combine the simplicity of the widget with the power of the [strong] shortcode into a single configuration screen. This new interface will also quicken the process of adding new features.', 'strong-testimonials' ); ?></p>
	
	<h4><?php _e( 'Display Mode - to show your testimonials', 'strong-testimonials' ); ?></h4>
	<ul>
		<li><?php _e( 'how many to show', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'which categories to include', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'the sort order', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'show the full content, the excerpt, or up to a specific length', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'which fields to include', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'the size of the Featured Image (thumbnail)', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'thumbnail link can open full-size image in a lightbox' ); ?></li>
		<li><?php _e( 'pagination', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'a "Read more" link to the full post or another page', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'choose a template, either included in the plugin or a custom template you create in your theme', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'set the background color', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'add CSS classes to match your theme', 'strong-testimonials' ); ?></li>
	</ul>
	
	<h4><?php _e( 'Slideshow Mode - to rotate through your testimonials', 'strong-testimonials' ); ?></h4>

	<p><?php _e( 'All the options of Display mode (except for pagination) plus:', 'strong-testimonials' ); ?></p>
	<ul>
		<li><?php _e( 'how long each slide is displayed', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'how long the transition effect lasts', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'whether to pause the slideshow when the mouse hovers over it', 'strong-testimonials' ); ?></li>
	</ul>

	<h4><?php _e( 'Form Mode - to add a testimonial submission form', 'strong-testimonials' ); ?></h4>
	<ul>
		<li><?php _e( 'assign new testimonials to specific categories', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'choose a template, either included in the plugin or a custom template you create in your theme', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'set the background color', 'strong-testimonials' ); ?></li>
		<li><?php _e( 'add CSS classes to match your theme', 'strong-testimonials' ); ?></li>
	</ul>

	<?php do_action( 'wpmtst_guide_after_content' ); ?>

</div>
