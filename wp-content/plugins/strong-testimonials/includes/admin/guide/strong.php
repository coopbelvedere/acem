<div class="guide-content strong-shortcode">
	<section>
		<h3><?php _e( 'The [strong] Shortcode', 'strong-testimonials' ); ?></h3>
		<p><?php _e( 'The <code>[strong]</code> shortcode is unique and versatile. Most attributes act as on/off switches. Think of it as constructing a sentence.', 'strong-testimonials' ); ?></p>
		<p><?php _e( 'If a page has multiple <code>[strong]</code>\'s, each one must be closed; e.g. <code>[strong &hellip; /]</code> or <code>[strong &hellip;][/strong]</code>.', 'strong-testimonials' ); ?></p>
		<table class="reference first wide">
			<?php include 'form.php'; ?>
			<?php include 'display.php'; ?>
			<?php include 'style.php'; ?>
			<?php include 'slideshow.php'; ?>
		</table>
		<table class="reference alternate wide">
			<?php include 'child.php'; ?>
		</table>
		<table class="reference wide">
			<?php include 'read_more.php'; ?>
		</table>
	</section>
</div>
