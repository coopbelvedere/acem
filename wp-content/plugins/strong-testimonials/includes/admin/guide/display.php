<!-- DISPLAY -->
<?php /* translators: In the [strong] shortcode instructions. */ ?>
<tr class="blank"><td colspan="3">&nbsp;</td></tr>
<tr class="heading">
	<th colspan="4"><?php _e( 'Display', 'strong-testimonials' ); ?></th>
</tr>

<?php include 'selection.php'; ?>
<?php include 'order.php'; ?>
<?php include 'content.php'; ?>
<?php include 'readmore.php'; ?>
<?php include 'pagination.php'; ?>
