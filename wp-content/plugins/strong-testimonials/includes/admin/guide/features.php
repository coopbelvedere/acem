<div class="guide-content features">

	<?php do_action( 'wpmtst_guide_before_content' ); ?>

	<h3><?php _e( 'New Features', 'strong-testimonials' ); ?></h3>

	<h4>Version 1.24</h4>
	<ul>
		<li>a new link field type for social media profiles</li>
		<li>better process for refreshing browser cache</li>
	</ul>

	<h4>Version 1.23</h4>

	<ul>
		<li>improved compatibility with Elegant Themes</li>
		<li>fully valid HTML</li>
	</ul>

	<h4>Version 1.22</h4>

	<ul>
		<li>support for Gravatars in Views, a long overdue feature</li>
	</ul>

	<h4>Version 1.21</h4>

	<ul>
		<li>WPML and Polylang compatible - so the form can be translated</li>
		<li>new templates included</li>
		<li>better template functions</li>
		<li>select from all available thumbnail sizes or set a custom size</li>
		<li>thumbnail link can open full-size image in a lightbox (try the <a href="https://wordpress.org/plugins/simple-colorbox/" target="_blank">Simple Colorbox</a> plugin)</li>
		<li>prevent double form submissions</li>
		<li>some admin speed improvement</li>
	</ul>

	<h4>Version 1.20</h4>

	<ul>
		<li>new shortcode option: more_page</li>
		<li>new child shortcode: [date]</li>
	</ul>

	<h4>Version 1.19</h4>

	<ul>
		<li>option to rename the [strong] shortcode</li>
	</ul>

	<h4>Version 1.18</h4>

	<ul>
		<li>option to send the notification email to multiple recipients</li>
		<li>option to add a category selector to the form</li>
	</ul>

</div>

<div class="guide-content coming-soon">

	<h3><?php _e( 'Premium Templates', 'strong-testimonials' ); ?></h3>

	<p>I am working on a premium add-on that will contain many modern templates to display your testimonials.</p>

	<h3><?php _e( 'Version 2.0', 'strong-testimonials' ); ?></h3>

	<p>Strong is my project for learning plugin development and it will continue to evolve. I do not believe in "if it ain't broke, don't fix it". I do not believe in "lite" versions. And I will continue to support you; as much as you allow, anyway.</p>

	<p>Version 2.0 will use more WordPress best practices (and cool tricks) and allow deeper customization and integration with themes and plugins. It will be stronger.</p>

	<p>Some feature candidates:</p>
	<ul>
		<li>star ratings</li>
		<li>slideshow controls and more effects</li>
		<li>microdata for improved SEO</li>
		<li>import/export</li>
	</ul>

	<?php do_action( 'wpmtst_guide_after_content' ); ?>

</div>
