<table class="matrix">
	<thead>
		<tr>
			<th class="top" rowspan="2" colspan="2">Attributes</th>
			<th colspan="3">Mode</th>
		</tr>
		<tr>
			<td>display</td>
			<td>slideshow</td>
			<td>form</td>
		</tr>
	</thead>
		
	<tbody>
		<tr>
			<th rowspan="3">Selection</th>
			<td>category</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
		</tr>
		<tr>
			<td>count</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>id</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		
		<tr>
			<th rowspan="4">Order</th>
			<td>random</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>newest</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>oldest</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>menu_order</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		
		<tr>
			<th rowspan="7">Content</th>
			<td>title</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>thumbnail</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
			<td>excerpt</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>length</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>more_post</i></td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>more_page</i></td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>more_text</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		
		<tr>
			<th rowspan="2">Pagination</th>
			<td>per_page</td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>nav</td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
			<td></td>
		</tr>

		<tr>
			<th rowspan="3">Slideshow</th>
			<td>show_for</td>
			<td></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>effect_for</td>
			<td></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>no_pause</i></td>
			<td></td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>

		<tr>
			<th rowspan="2">Style</th>
			<td>template</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
		</tr>
		<tr>
			<td>class</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
		</tr>
	</tbody>
</table>

<table class="matrix">
	<thead>
		<tr>
			<th rowspan="2">Attributes</th>
			<th colspan="3">Child Shortcodes</th>
		</tr>
		<tr>
			<td>&#91;client&#93;</td>
			<td>&#91;field&#93;</td>
			<td>&#91;date&#93;</td>
		</tr>
	</thead>
	
	<tbody>
		<tr>
			<td>name</td>
			<td class="text" rowspan="5">no attributes yet &ndash;<br>only used as a container for<br>&#91;field&#93; and &#91;date&#93;</td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>url</td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>new_tab</td>
			<td><i class="fa fa-check"></i></td>
			<td></td>
		</tr>
		<tr>
			<td>class</td>
			<td><i class="fa fa-check"></i></td>
			<td><i class="fa fa-check"></i></td>
		</tr>
		<tr>
			<td>format</td>
			<td></td>
			<td><i class="fa fa-check"></i></td>
		</tr>
	</tbody>
</table>
