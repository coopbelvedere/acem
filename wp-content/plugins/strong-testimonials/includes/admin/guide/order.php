<!-- ORDER -->
<tr class="section">
	<th colspan="4">
		<h4><?php _e( 'Order', 'strong-testimonials' ); ?></h4>
	</th>
</tr>
<tr class="sub">
	<th>attribute</th>
	<th>purpose</th>
	<th>default</th>
	<th>accepted values</th>
</tr>
<tr class="att">
	<td>oldest</td>
	<td><?php _e( 'to show from oldest to newest', 'strong-testimonials' ); ?></td>
	<td><span class="dashicons dashicons-yes"></span></td>
	<td></td>
</tr>
<tr class="att">
	<td>newest</td>
	<td><?php _e( 'to show from newest to oldest', 'strong-testimonials' ); ?></td>
	<td></td>
	<td></td>
</tr>
<tr class="att">
	<td>random</td>
	<td><?php _e( 'to show in random order', 'strong-testimonials' ); ?></td>
	<td></td>
	<td></td>
</tr>
<tr class="att">
	<td>menu_order</td>
	<td><?php _e( 'to show in menu order', 'strong-testimonials' ); ?></td>
	<td></td>
	<td></td>
</tr>

<tr class="example">
	<td colspan="4" class="has-inner">
		<table>
			<tr class="sub">
				<th><?php _e( 'examples', 'strong-testimonials' ); ?></th>
			</tr>
			<tr>
				<td>[strong newest &hellip;]</td>
			</tr>
			<tr>
				<td>[strong random &hellip;]</td>
			</tr>
		</table>
	</td>
</tr>
