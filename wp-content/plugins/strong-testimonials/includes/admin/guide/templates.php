<div class="guide-content templates">

	<?php do_action( 'wpmtst_guide_before_content' ); ?>


	<h3><?php _e( 'Template files', 'strong-testimonials' ); ?></h3>

	<p><?php _e( 'The plugin includes new templates for use in Views.', 'strong-testimonials' ); ?></p>

	<p><?php _e( 'The new <strong>Default</strong> template is the same as the <code>[strong]</code> shortcode template except without the gradient gray background.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'The <strong>Simple</strong> template has minimal style and may be the best option for matching your theme.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'The <strong>Widget</strong> template has smaller sizing for use in a sidebar or other widget area.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'The <strong>Unstyled</strong> template includes no stylesheet and is a good foundation for theme experts.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'For all templates, you can set the background color in the View settings.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'Feel free to <a href="https://www.wpmission.com/contact" target="_blank">contact me</a> for help with minor style changes to add to a <a href="https://thethemefoundry.com/blog/wordpress-child-theme/" target="_blank">child theme</a> or <a href="https://wordpress.org/plugins/so-css/" target="_blank">custom CSS</a>.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'New templates may be added occasionally. A premium add-on with a variety of modern templates will be available soon.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( '<strong>Version 1.21 deprecates using custom template files with the <code>[strong]</code> shortcode.</strong> If you are already using a custom template with that shortcode, it will still work, though I encourage you to move up to Views instead.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'New template functions are also available.', 'strong-testimonials' ); ?></p>
	
	<p><?php _e( 'Visit <a href="https://www.wpmission.com/strong-testimonials" target="_blank">WP Mission</a> for more information on customizing templates.', 'strong-testimonials' ); ?></p>


	<?php do_action( 'wpmtst_guide_after_content' ); ?>

</div>
