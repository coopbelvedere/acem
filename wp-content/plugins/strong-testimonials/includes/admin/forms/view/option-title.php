<th>
	<input type="checkbox" id="view-title" name="view[data][title]" value="1" <?php checked( $view['title'] ); ?> class="checkbox">
	<label for="view-title">
		<?php _e( 'Title', 'strong-testimonials' ); ?>
	</label>
</th>
<td colspan="2">
	<div class="inline">
		<p class="description">
			<?php _e( '(if included in Fields)', 'strong-testimonials' ); ?>
		</p>
	</div>
</td>
