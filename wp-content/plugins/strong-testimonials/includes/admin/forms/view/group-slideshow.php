<?php /* translators: On the Views admin screen. */ ?>
<div class="then then_not_display then_not_form then_slideshow then_not_single" style="display: none;">
	<h3><?php _e( 'Slideshow', 'strong-testimonials' ); ?></h3>
	<table class="form-table multiple group-select" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<?php include( 'mode-slideshow.php' ); ?>
		</tr>
	</table>
</div>
