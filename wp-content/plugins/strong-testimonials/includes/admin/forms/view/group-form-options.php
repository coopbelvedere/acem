<?php /* translators: On the Views admin screen. */ ?>
<div class="then then_not_display then_not_slideshow then_form" style="display: none;">
	<div>
		<h3><?php _e( 'Options', 'strong-testimonials' ); ?></h3>
	</div>
	<table class="form-table multiple group-select" cellpadding="0" cellspacing="0">
		<tr>
			<?php include( 'option-category-form.php' ); ?>
		</tr>
	</table>
</div>
