<th>
	<label for="view-background"><?php _e( 'Background Color', 'strong-testimonials' ); ?></label>
</th>
<td>
	<div style="display: none;" class="then then_display then_form then_slideshow input">
		<input type="text" id="view-background" name="view[data][background]" value="<?php echo $view['background']; ?>" class="wp-color-picker-field" autocomplete="off">
	</div>
</td>
