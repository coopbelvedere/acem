<th>
	<label for="view-class"><?php _e( 'CSS Classes', 'strong-testimonials' ); ?></label>
</th>
<td>
	<div style="display: none;" class="then then_display then_form then_slideshow input">
		<input type="text" id="view-class" class="long" name="view[data][class]" value="<?php echo $view['class']; ?>" autocomplete="off">
		<p class="inline description"><?php _e( 'separated by commas or spaces', 'strong-testimonials' ); ?></p>
	</div>
</td>
