<?php
/**
 * The form.
 * 
 * @param $atts
 *
 * @return mixed|string|void
 */
function wpmtst_form_shortcode( $atts ) {
	
	if ( isset( $_GET['success'] ) )
		return '<div class="testimonial-success">' . wpmtst_get_form_message( 'submission-success' ) . '</div>';
	
	//TODO trim down atts?
	//extract( normalize_empty_atts( $atts ) );
	$atts = normalize_empty_atts( $atts );

	// initialize
	$field_options       = get_option( 'wpmtst_fields' );
	$field_groups        = $field_options['field_groups'];
	$current_field_group = $field_groups[ $field_options['current_field_group'] ];
	$fields              = $current_field_group['fields'];

	$form_values = array( 'category' => $atts['category'] );
	foreach ( $fields as $key => $field ) {
		$form_values[ $field['name'] ] = '';
	}
	$previous_values = WPMST()->get_form_values();
	if ( $previous_values ) {
		$form_values = array_merge( $form_values, $previous_values );
	}
	WPMST()->set_form_values( $form_values );

	$template_file = wpmtst_find_form_template( $atts['template'], $atts['view'] );

	ob_start();
	include $template_file;
	$html = ob_get_contents();
	ob_end_clean();

	$html = apply_filters( 'strong_html', $html );
	return $html;
}

/**
 * Honeypot preprocessor
 */
function wpmtst_honeypot_before() {
	if ( isset( $_POST['wpmtst_if_visitor'] ) && ! empty( $_POST['wpmtst_if_visitor'] ) ) {
		do_action( 'honeypot_before_spam_testimonial', $_POST );
		$form_options = get_option( 'wpmtst_form_options' );
		$messages     = $form_options['messages'];
		die( apply_filters( 'wpmtst_l10n', $messages['submission-error']['text'], wpmtst_get_l10n_context( 'form-messages' ), $messages['submission-error']['description'] ) );
	}
	return;
}


/**
 * Honeypot preprocessor
 */
function wpmtst_honeypot_after() {
	if ( ! isset ( $_POST['wpmtst_after'] ) ) {
		do_action( 'honeypot_after_spam_testimonial', $_POST );
		$form_options = get_option( 'wpmtst_form_options' );
		$messages     = $form_options['messages'];
		die( apply_filters( 'wpmtst_l10n', $messages['submission-error']['text'], wpmtst_get_l10n_context( 'form-messages' ), $messages['submission-error']['description'] ) );
	}
	return;
}


/**
 * Honeypot
 */
function wpmtst_honeypot_before_script() {
	?>
<script type="text/javascript">jQuery('#wpmtst_if_visitor').val('');</script>
	<?php 
}


/**
 * Honeypot
 */
function wpmtst_honeypot_after_script() {
	?>
<script type='text/javascript'>
	//<![CDATA[
	( function( $ ) {
		'use strict';
		var forms = "#wpmtst-submission-form";
		$( forms ).submit( function() {
			$( "<input>" ).attr( "type", "hidden" )
			.attr( "name", "wpmtst_after" )
			.attr( "value", "1" )
			.appendTo( forms );
			return true;
		});
	})( jQuery );
	//]]>
</script>
	<?php
}

/**
 * Submission form validation.
 *
 * Required for original shortcodes.
 * To be removed in 2.0.
 */
function wpmtst_validation_function() {
	echo "\r"; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#wpmtst-submission-form").validate({});
	});
</script>
	<?php
}

/**
 * ---------------------------------------------------------------------------
 * 
 *     FORM TEMPLATE FUNCTIONS
 * 
 * ---------------------------------------------------------------------------
 */

function wpmtst_form_info() {
	// action="' . admin_url( 'admin-post.php' ) . '"
	echo 'id="wpmtst-submission-form" method="post" enctype="multipart/form-data"';
}


function wpmtst_form_hidden_fields() {
	$form_values = WPMST()->get_form_values();
	echo '<input type="hidden" name="action" value="wpmtst_form">';
	echo '<input type="hidden" name="category" value="'. $form_values['category'] .'">';

}

function wpmtst_form_nonce() {
	wp_nonce_field( 'wpmtst_form_action', 'wpmtst_form_nonce', true, true );
}

function wpmtst_form_message( $part ) {
	echo wpmtst_get_form_message( $part );
}

function wpmtst_get_form_message( $part ) {
	$form_options = get_option( 'wpmtst_form_options' );
	$messages = $form_options['messages'];
	if ( isset( $messages[$part]['text'] ) ) {
		return apply_filters( 'wpmtst_l10n', $messages[$part]['text'], wpmtst_get_l10n_context( 'form-messages' ), $messages[$part]['description'] );
	}
}

function wpmtst_all_form_fields() {
	$field_options       = get_option( 'wpmtst_fields' );
	$field_groups        = $field_options['field_groups'];
	$current_field_group = $field_groups[ $field_options['current_field_group'] ];
	$fields              = $current_field_group['fields'];
	foreach ( $fields as $key => $field ) {
		wpmtst_single_form_field( $field );
	}
}

function wpmtst_form_field( $field_name ) {
	$field_options       = get_option( 'wpmtst_fields' );
	$field_groups        = $field_options['field_groups'];
	$current_field_group = $field_groups[ $field_options['current_field_group'] ];
	$fields              = $current_field_group['fields'];
	foreach ( $fields as $key => $field ) {
		if ( $field['name'] == $field_name ) {
			wpmtst_single_form_field( $field );
		}
	}
}

function wpmtst_single_form_field( $field ) {
	$form_values = WPMST()->get_form_values();
	$context = wpmtst_get_l10n_context( 'form-fields' );

	echo '<p class="form-field">';
	echo '<label for="wpmtst_' . $field['name'] . '">' . apply_filters( 'wpmtst_l10n', $field['label'], $context, $field['name'] ) . '</label>';

	wpmtst_field_required_symbol( $field );
	wpmtst_field_before( $field );
	switch ( $field['input_type'] ) {

		case 'categories':
			$value = isset( $form_values[ $field['name'] ] ) ? $form_values[ $field['name'] ] : '';
			$category_list = get_terms( 'wpm-testimonial-category', array(
				'hide_empty' => false,
				'order_by'   => 'name',
			) );
			echo '<select id="wpmtst_' . $field['name']. '"'
				. ' name="' . $field['name'] . '"'
				. ' class="' . wpmtst_field_classes( $field['input_type'], $field['name'] ) . '"'
				. wpmtst_field_required_tag( $field ) . ' autocomplete="off">';
			echo '<option value="">&mdash;</option>';
			foreach ( $category_list as $category ) {
				echo '<option value="' . $category->term_id . '" ' . selected( $category->term_id, $value ) . '>'; 
				echo $category->name;
				echo '</option>';
			}
			echo '</select>';
			break;
		
		case 'textarea':
			$value = ( isset( $form_values[ $field['name'] ] ) && $form_values[ $field['name'] ] ) ? $form_values[ $field['name'] ] : '';
			// textarea tags must be on same line for placeholder to work
			echo '<textarea id="wpmtst_' . $field['name'] . '"'
			     . ' class="' . wpmtst_field_classes( $field['input_type'], $field['name'] ) . '"'
			     . ' name="' . $field['name'] . '"'
			     . wpmtst_field_required_tag( $field ) . wpmtst_field_placeholder( $field )
			     . '>' . $value . '</textarea>';
			break;

		case 'file':
			echo '<input id="wpmtst_' . $field['name'] . '" type="file" name="' . $field['name'] . '">';
			break;

		default: // test, email, url
			/**
			 * Switching out url type until more themes adopt it.
			 * @since 1.11.0
			 */
			$input_type = ( $field['input_type'] = 'url' ? 'text' : $field['input_type'] );
			$value = isset( $form_values[ $field['name'] ] ) ? $form_values[ $field['name'] ] : '';
			echo '<input id="wpmtst_' . $field['name'] . '"'
			     . ' type="' . $input_type . '"'
			     . ' class="' . wpmtst_field_classes( $field['input_type'], $field['name'] ) . '"'
			     . ' name="' . $field['name'] . '"'
			     . ' value="' . $value . '"'
			     . wpmtst_field_placeholder( $field ) . wpmtst_field_required_tag( $field )
			     . '>';
	}
	wpmtst_field_error( $field );
	wpmtst_field_after( $field );
	echo '</p>';
}

function wpmtst_field_classes( $type = null, $name = null ) {
	$errors = WPMST()->get_form_errors();
	$class_list = array();

	switch( $type ) {
		case 'email':
			$class_list[] = 'text';
			$class_list[] = 'email';
			break;
		case 'url':
			$class_list[] = 'text';
			$class_list[] = 'url';
			break;
		case 'text':
			$class_list[] = 'text';
			break;
		default:
			break;
	}

	if ( isset( $errors[ $name ] ) ) {
		$class_list[] = 'error';
	}

	return apply_filters( 'wpmtst_form_field_class', implode( ' ', $class_list ), $type, $name );
}

function wpmtst_field_placeholder( $field ) {
	if ( isset( $field['placeholder'] ) && $field['placeholder'] )
		return ' placeholder="' . $field['placeholder'] . '"';
}

function wpmtst_field_required_tag( $field ) {
	if ( isset( $field['required'] ) && apply_filters( 'wpmtst_field_required_tag', $field['required'] ) )
		return ' required';
}

function wpmtst_field_required_symbol( $field ) {
	if ( isset( $field['required'] ) && $field['required'] )
		echo '<span class="required symbol"></span>';
}

function wpmtst_field_before( $field ) {
	if ( isset( $field['before'] ) && $field['before'] ) {
		$context = wpmtst_get_l10n_context( 'form-fields' );
		echo '<span class="before">' . apply_filters( 'wpmtst_l10n', $field['before'], $context, $field['name'] . ' : before' ) . '</span>';
	}
}

function wpmtst_field_after( $field ) {
	if ( isset( $field['after'] ) && $field['after'] ) {
		$context = wpmtst_get_l10n_context( 'form-fields' );
		//echo '<span class="after">' . $field['after'] . '</span>';
		echo '<span class="after">' . apply_filters( 'wpmtst_l10n', $field['after'], $context, $field['name'] . ' : after' ) . '</span>';
	}
}

function wpmtst_field_error( $field ) {
	$errors = WPMST()->get_form_errors();
	if ( isset( $errors[ $field['name'] ] ) )
		echo '<span class="error">' . $errors[ $field['name'] ] . '</span>';
}

function wpmtst_form_honeypot_before() {
	$form_options = get_option( 'wpmtst_form_options' );
	if ( $form_options['honeypot_before'] ) {
		?>
		<style>#wpmtst-form .wpmtst_if_visitor * { display: none !important; visibility: hidden !important; }</style>
		<span class="wpmtst_if_visitor"><label for="wpmtst_if_visitor">Visitor?</label><input id="wpmtst_if_visitor" type="text" name="wpmtst_if_visitor" size="40" tabindex="-1" autocomplete="off"></span>
		<?php
	}
}
add_action( 'wpmtst_form_after_fields', 'wpmtst_form_honeypot_before' );

function wpmtst_form_captcha() {
	$errors = WPMST()->get_form_errors();
	$form_options = get_option( 'wpmtst_form_options' );
	if ( $form_options['captcha'] ) {
		// Only display Captcha label if properly configured.
		$captcha_html = apply_filters( 'wpmtst_captcha', $form_options['captcha'] );
		if ( $captcha_html ) {
			?>
			<div class="wpmtst-captcha">
				<label for="wpmtst_captcha"><?php wpmtst_form_message('captcha'); ?></label><span class="required symbol"></span>
				<div>
					<?php echo $captcha_html; ?>
					<?php if ( isset( $errors['captcha'] ) ) : ?>
						<p><label class="error"><?php echo $errors['captcha']; ?></label></p>
					<?php endif; ?>
				</div>
			</div>
			<?php
		}
	}

}
add_action( 'wpmtst_form_after_fields', 'wpmtst_form_captcha' );

function wpmtst_form_submit_button() {
	$form_options = get_option( 'wpmtst_form_options' );
	$messages     = $form_options['messages'];
	?>
	<p class="form-field submit">
		<input type="submit" id="wpmtst_submit_testimonial" name="wpmtst_submit_testimonial" value="<?php echo $messages['form-submit-button']['text']; ?>" class="button">
	</p>
	<?php
	// validate="required:true"
}
