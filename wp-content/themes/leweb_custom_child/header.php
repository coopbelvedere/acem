<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php wp_title(''); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

	<style>
		<?php
		$sectionColor = "#444";
		if (is_page_child(431) or is_page_child(620)) {
			// Financement
			$sectionColor = "#aebd14";
		} else if (is_page_child(433) or is_page_child(622)) {
			// Formation
			$sectionColor = "#5772a3";
		} else if (is_page_child(437) or is_page_child(630)) {
			// Investir
			$sectionColor = "#838487";
		} else if (is_page_child(435) or is_page_child(634)) {
			// Reconnaissance des acquis
			$sectionColor = "#ee7203";
		}
		echo "#page.container { border-top: 4px solid {$sectionColor}} ";
		echo ".section-border { border-top: 4px solid {$sectionColor}} ";
		echo ".section-color {color: {$sectionColor}} ";
		echo ".section-background { background-color: {$sectionColor}} ";
		?>
	</style>

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>
	<meta name="viewport" content="initial-scale=1, width=900">
</head>

<body <?php body_class(); ?>>

<div id="wrapper">

	<header id="header">

		<?php if ( has_nav_menu('topbar') ): ?>
			<nav class="nav-container group" id="nav-topbar">
				<div class="nav-toggle"><i class="fa fa-bars"></i></div>
				<div class="nav-text"><!-- put your mobile menu text here --></div>
				<div class="nav-wrap container">
					<?php wp_nav_menu(array('theme_location'=>'topbar','menu_class'=>'nav','container'=>'','menu_id' => '','fallback_cb'=> false)); ?>
					<?php //do_action('icl_language_selector'); ?>
					<div class="toggle-search"><i class="fa fa-search"></i></div>
					<div class="search-expand">
						<div class="search-expand-inner">
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>

			</nav><!--/#nav-topbar-->
		<?php endif; ?>

		<div style="margin-top: -30px;" class="container group">
			<div class="container-inner">


				<?php if ( ot_get_option('header-image') ): ?>
					<a href="<?php echo home_url('/'); ?>" rel="home">
						<img class="site-image" src="<?php echo ot_get_option('header-image'); ?>" alt="<?php get_bloginfo('name'); ?>">
					</a>
				<?php endif; ?>

				<?php if ( has_nav_menu('header') ): ?>
					<nav class="nav-container group" id="nav-header">
						<div class="nav-toggle"><i class="fa fa-bars"></i></div>
						<div class="nav-text"><!-- put your mobile menu text here --></div>
						<div class="nav-wrap container">
							<?php $loc_path = (ICL_LANGUAGE_CODE == 'en') ? '/en/' : '/'; ?>
                            <a id="logo-acem" href="<?= $loc_path ?>" >
                                <img src="/wp-content/uploads/2014/09/acem.png"/>
                            </a>
                            <?php wp_nav_menu(array('theme_location'=>'header','menu_class'=>'nav container-inner group','container'=>'','menu_id' => '','fallback_cb'=> false)); ?>
                            <div id="sub-menu-box">
                                <a href="https://www.facebook.com/acemcreditcommunautaire" target="_blank"><img src="/wp-content/uploads/2014/09/social_icon_facebook.png"/></a>
                                <a href="https://twitter.com/ACEM_FCR" target="_blank"><img src="/wp-content/uploads/2014/09/social_icon_twitter.png"/></a>
                                <a href="https://www.linkedin.com/company/acem-objectif-reconnaissance" target="_blank"><img src="/wp-content/uploads/2014/09/social_icon_likedin.png"/></a>
                                <!--<a href="#"><img src="/wp-content/uploads/2014/09/social_icon_RSS.png"/></a>-->
                            </div>
                        </div> <!-- nav-wrap container -->
					</nav><!--/#nav-header-->
				<?php endif; ?>

			</div><!--/.container-inner-->
		</div><!--/.container-->

	</header><!--/#header-->

	<div class="container" id="page">
		<div class="container-inner">
			<div class="main">
				<div class="main-inner group">
