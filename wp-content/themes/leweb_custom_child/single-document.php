<?php
/** Template pour l'affichage des catégories de documents */
?>

<?php get_header();
$docuCenterLink = __('/centre-de-documentation/', 'acem-taxo-cat-documentation');
?>
<div class="cols clearfix centre-documentation">
	<h1 class="page-title"><a href="<?= $docuCenterLink ?>">Documents</a> » <?= the_title() ?></h1>

	<div class="document no-col">
		<?php
		if (have_posts()) {
			echo "<div class='items'>";

			while (have_posts()) {
				the_post();
				$id = get_the_ID();
				$doctype = get_field('doctype', $id);
				$description = get_the_content();

				if ($doctype == 'file' or $doctype == '') { // '' for legacy purpose, when field didn't exist
					$file = get_field('file',$id);
					$ext = mb_strtoupper(pathinfo($file['filename'], PATHINFO_EXTENSION));
					$extString = sprintf(__('Document %s', 'acem-single-document'), $ext);
					$permalink = $file['url'];
					?>
					<div class="item">
						<div class="publish-date">
							<?= __('Publié le', 'acem-single-document') ?> :
							<?= get_the_date('Y-m-d') ?>
						</div>
						<div class="ext">Type: <?= $extString ?></div>
						<?php get_section_tags() ?>
						<a href="<?= $permalink ?>" target="_blank" class="download"><?= __('Télécharger le document', 'acem-single-document') ?></a>
						<div class="description"><?= $description ?></div>
					</div>
					<?php
				} elseif ($doctype == 'url') {
					$url = get_field('url',$id);
					?>
					<div class="item">
						<div class="publish-date">
							<?= __('Publié le', 'acem-single-document') ?> :
							<?= get_the_date('Y-m-d') ?>
						</div>
						<div class="ext">Type: <?= __('Hyperlien', 'acem-single-document') ?></div>
						<?php get_section_tags() ?>
						<a href="<?= $url ?>" target="_blank" class="download"><?= __('Afficher le document', 'acem-single-document') ?></a>
						<div class="description"><?= $description ?></div>
					</div>
					<?php
				}
			}

			echo "</div>";
		}
		?>
	</div>
</div>
<?php get_footer(); ?>
