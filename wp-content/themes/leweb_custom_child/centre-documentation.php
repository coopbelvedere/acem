<?php
/**
 * Template Name: Centre documentation
 *
 */
?>

<?php get_header(); ?>
<div class="cols clearfix centre-documentation">
	<h1 class="page-title"><?= get_the_title() ?></h1>

	<div class="content">
	<?= the_content() ?>
	</div>

	<div class="documents">
	<?php
	$terms = get_terms('categorie-documentation');

	foreach ($terms as $term) {
		$docs = get_posts(array(
				'post_type' => 'document',
				'posts_per_page' => 5,
				'tax_query' => array(
					array(
						'taxonomy' => 'categorie-documentation',
						'terms' => $term,
					)
				)
			)
		);

		$lang = (ICL_LANGUAGE_CODE == 'en') ? '/en' : null;

		$url = "{$lang}/{$term->taxonomy}/{$term->slug}/";

		if (!empty($docs)) {
			echo "<div class='items'>";
			echo "<h2><a href='{$url}'>{$term->name}</a></h2>";
			echo "<a href='{$url}' class='all'>".__('Voir tous les documents de ce type', 'acem-centre-documentation')." »</a>";
			foreach ($docs as $doc) {
				$id = $doc->ID;
				$file = get_field('file',$id);
				$ext = mb_strtoupper(pathinfo($file['filename'], PATHINFO_EXTENSION));
				$extString = sprintf(__('Document %s'), $ext, 'acem-centre-documentation');
				$permalink = get_permalink($doc);
				$description = wp_html_excerpt($doc->post_content, 60) . '...';
				?>
				<div class="item">
					<a href="<?= $permalink ?>" class="title"><?= $doc->post_title ?></a>
					<div class="ext"><?= date('Y-m-d',strtotime($doc->post_date)) ?> &mdash; <?= $extString ?></div>
					<div class="description"><?= $description ?></div>
					<a href="<?= $permalink ?>" class="link"><?= __('Voir le document', 'acem-centre-documentation') ?> »</a>
				</div>
				<?php
			}
			echo "</div>";
		}
	}
	?>
	</div>
</div>
<?php get_footer(); ?>
