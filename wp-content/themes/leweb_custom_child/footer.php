				</div><!--/.main-inner-->
			</div><!--/.main-->
		</div><!--/.container-inner-->
	</div><!--/.container-->



</div><!--/#wrapper-->
<div data-debug="fkdlsfjds" class="bande-couleur"></div>
<footer id="footer">
	<div class="footer-content">
		<div class="infos">
			<img class="logo" src="/wp-content/uploads/2015/10/logo-acem.png" alt="">
			<div class="sitename"><?= __('acem crédit communautaire', 'acem-footer') ?></div>
			<div class="address">
				<p>
					<?= __('3680, rue Jeanne-Mance, Bureau 319', 'acem-footer') ?><br/>
					<?= __('Montréal (Québec) H2X 2K5', 'acem-footer') ?><br/>
				</p>
				<p>
					<?= __('Tél: 514 843-7296', 'acem-footer') ?><br/>
					<?= __('Fax: 514 843-6832', 'acem-footer') ?><br/>
				</p>
				<p>
					<a href="mailto:info@acem-montreal.ca">info@acem-montreal.ca</a>
				</p>
			</div>
		</div>

		<div class="footer-menu">
			<h1><?= __("Plan du site", 'acem-footer') ?></h1>
			<div class="cols">
			<?php wp_nav_menu(array('menu' => 'footer-menu'))?>
			</div>
		</div>

		<div class="social">
			<h1><?= __("Restez connecté", 'acem-footer') ?></h1>

			<a class="social-icon" href="https://www.facebook.com/acemcreditcommunautaire" target="_blank"><i class="fa fa-facebook-square"></i></a>
			<a class="social-icon" href="https://twitter.com/ACEM_FCR" target="_blank"><i class="fa fa-twitter"></i></a>
			<a class="social-icon" href="https://www.linkedin.com/company/acem-objectif-reconnaissance" target="_blank"><i class="fa fa-linkedin"></i></a>


			<a class="infolettre" href="http://acemcreditcommunautaire.us8.list-manage1.com/subscribe?u=b36c44c197b317a80503397d5&id=7440093896" target="_blank">
				<img src="/wp-content/uploads/2015/10/icone-infolettre.png" alt="Infolettre">
				<?= __("S'abonner à l'infolettre l'ACEMence", 'acem-footer')?>
			</a>

			<p style="text-transform: uppercase; font-weight: bold;"><?= __("Membre du", 'acem-footer') ?></p>
			<?php
			$logoHome = get_field('logo_home', 27);
			$logoOther = get_field('logo_other', 27);
			$logoHome = $logoHome['url'];
			$logoOther = $logoOther['url'];
			if (is_page(27) or is_page(507)) {
				$logoUrl = $logoHome;
			} else {
				$logoUrl = $logoOther;
			}
			?>
			<p><a class="logo-rqcc" href="http://www.rqcc.qc.ca" target="_blank"><img src="<?= $logoUrl ?>" alt=""></a></p>
		</div>
	</div>

	<section id="footer-bas">
		&copy; <?= date('Y') ?> <?= __('ACEM. Tous droits réservés.', 'acem-footer') ?> &nbsp;&nbsp;|&nbsp;&nbsp; <?= __('Réalisé par', 'acem-footer') ?> <a style="color:white" href="http://coopbelvedere.com/" target="_blank">Coopérative Belvédère Communication</a>
	</section><!--/.container-->
</footer><!--/#footer-->

<?php wp_footer(); ?>
</body>
</html>
