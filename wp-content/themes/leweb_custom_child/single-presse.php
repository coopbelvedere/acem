<?php
/** Template pour l'affichage unique des éléments de presse */
?>

<?php get_header();

?>
<div class="cols clearfix single-presse">
	<h1 class="page-title"><?= get_press_room_url() ?><a href="<?= get_post_type_archive_link('presse') ?>"><?= __('Revue de presse', 'acem-single-presse') ?></a> » <?= the_title() ?></h1>

	<div class="no-col">
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				$id = get_the_ID();

				$date = date('Y-m-d',strtotime(get_the_date('Y-m-d')));
				$excerpt = get_field('excerpt', $id);
				$type = get_field('type', $id);
				if ($type == 'document') {
					$typeMessage = __('Télécharger le document', 'acem-single-presse');
					$file = get_field('document', $id);
					$url = $file['url'];
				} elseif ($type == 'link') {
					$typeMessage = __('Ouvrir le lien', 'acem-single-presse');
					$link = get_field('link', $id);
					$url = $link;
				}
				$permalink = get_permalink($id);

				?>
				<div class="infos"><?= __('Date de publication :', 'acem-single-presse') ?> <?= $date ?></div>
				<a href="<?= $url ?>" class="download" target="_blank"><?= $typeMessage ?></a>
				<div class="text content">
					<?= $excerpt ?>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>
<?php get_footer(); ?>
