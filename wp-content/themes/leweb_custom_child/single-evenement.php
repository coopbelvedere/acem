<?php
/** Template pour l'affichage unique des évènement */
?>

<?php get_header();

?>
<div class="cols clearfix evenement">
	<h1 class="page-title"><a href="<?= get_post_type_archive_link('evenement') ?>"><?= __('Évènements', 'single-evenement') ?></a> » <?= the_title() ?></h1>

	<div class="no-col">
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				$id = get_the_ID();

				$description = get_field('description',$id);
				$date = get_field('date',$id);
				$all_day = get_field('all_day',$id);
				$start_time = get_field('start_time',$id);
				$end_time = get_field('end_time',$id);
				$email = get_field('email',$id);
				$reservation = get_field('reservation',$id);
				$address_line_one = get_field('address_line_one',$id);
				$address_line_two = get_field('address_line_two',$id);
				$image = get_field('image',$id);
				if (isset($image)) {
					$image = $image['sizes']['thumb-large'];
				}

				if ($all_day) {
					$time = __('Toute la journée', 'single-evenement');
				} elseif (!empty($start_time) and empty($end_time)) {
					$time = $start_time;
				} elseif (!empty($start_time) and !empty($end_time)) {
					$time = $start_time . ' - ' . $end_time;
				}

				?>

				<div class="masthead clearfix">
					<div class="infos">
						<div class="contents">
							<h2>Informations</h2>
							<div class="date clearfix"><?= __('Date:', 'single-evenement') ?> <span class="value"><?= $date ?></span></div>
							<div class="time clearfix"><?= __('Heure:', 'single-evenement') ?> <span class="value"><?= $time ?></span></div>
							<?php if (!empty($address_line_one)) { ?>
								<div class="address_one clearfix"><?= __('Lieu:', 'single-evenement') ?> <span class="value"><?= $address_line_one ?></span></div>
								<?php if (!empty($address_line_two)) { ?>
									<div class="address_two clearfix"> <span class="value"><?= $address_line_two ?></span></div>
								<?php } ?>
							<?php } ?>
							<?php if (!empty($email)) { ?>
								<div class="contact clearfix"><?= __('Contact:', 'single-evenement') ?> <span class="value"><a href="mailto:<?= $email ?>"><?= $email ?></a></span></div>
							<?php } ?>
							<?php if (!empty($reservation)) { ?>
								<div class="reservation clearfix"><?= __('Réservation:', 'single-evenement') ?> <span class="value"><?= $reservation ?></span></div>
							<?php } ?>
						</div>
					</div>

					<div class="spacer"></div>

					<div class="image" style="background-image:url('<?= $image ?>')">
					</div>
				</div>

				<div class="description">
					<h2 class="underlined">Description</h2>
					<?= $description ?>
				</div>

				<p><br /><?php get_section_tags(); ?></p>
				<?php
			}
		}
		?>
	</div>
</div>
<?php get_footer(); ?>
