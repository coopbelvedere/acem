<?php
/**
 * Template Name: Section
 *
 */
?>

<?php
get_header();

?>

<div class="testimonials-margins section-border">
	<div class="testimonials-header">
		<h1><?= __('Témoignages', 'acem-section') ?></h1>
	</div>
	<?php get_slider_testimonials() ?>
</div>

<div class="cols clearfix">
	<?php get_nouvelles() ?>

	<?php get_documents() ?>
</div>

<div class="evenements full-width section-border">
<h1><?= __('Prochains évènements', 'acem-section') ?></h1>
	<?php get_section_events() ?>
</div>

<?php get_slider_partenaires() ?>

<script type="text/javascript">
	$ = jQuery;
	$(document).ready(function(){
		$('.slider-contributeurs').slick({
			autoplay: true,
			autoplaySpeed: 3000,
			variableWidth: true,
			infinite: true,
			arrows: false,
			slidesToShow: 1
		});
	});
</script>

<?php get_footer(); ?>
