<?php get_header(); ?>

<section class="content">

	<?php get_template_part('inc/page-title'); ?>

	<div class="group">

		<?php while ( have_posts() ): the_post(); ?>

			<article <?php post_class('group'); ?>>

				<?php get_template_part('inc/page-image'); ?>

				<div class="entry themeform">
					<?php
					$tocActive = get_field('toc_active');

					if ($tocActive) {
						$content = apply_filters( 'the_content', get_the_content() );
						$content = str_replace( ']]>', ']]&gt;', $content );
						echo "<div class='text'>";
						get_text_with_toc($content);
						echo "</div>";
					} else {
						echo "<div class='text'>";
						the_content();
						echo "</div>";
					}
					?>
					<div class="clear"></div>
				</div><!--/.entry-->

			</article>

			<?php if ( ot_get_option('page-comments') == 'on' ) { comments_template('/comments.php',true); } ?>

		<?php endwhile; ?>

	</div><!--/.pad-->

</section><!--/.content-->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
