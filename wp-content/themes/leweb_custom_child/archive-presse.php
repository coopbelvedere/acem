<?php

?>

<?php get_header(); ?>
<div class="cols clearfix archive-presse">
	<h1 class="page-title"><?= get_press_room_url() ?><?= __('Revue de presse', 'acem-archive-presse') ?></h1>

	<?php
	if (have_posts()) {
		echo "<div class='press-releases'>";

		while (have_posts()) {
			the_post();
			$id = get_the_ID();

			$title = get_the_title();
			$date = get_the_date('Y-m-d');
			$excerpt = wp_html_excerpt(get_field('excerpt', $id), 350, "...");
			$type = get_field('type', $id);
			if ($type == 'document') {
				$typeMessage = __('Document téléchargable', 'acem-archive-presse');
			} elseif ($type == 'link') {
				$typeMessage = __('Ressource Web', 'acem-archive-presse');
			}
			$permalink = get_permalink($id);
			?>
			<div class="item">
				<a href="<?= $permalink ?>" class="title"><?= $title ?></a>
				<div class="infos"><?= $date ?> | <?= $typeMessage ?></div>
				<div class="excerpt"><?= $excerpt ?></div>
				<a href="<?= $permalink ?>" class="more"><?= __('Voir les détails', 'acem-archive-presse') ?> »</a>
			</div>
			<?php
		}
		echo "</div>";
	}
	?>
</div>
<?php get_footer(); ?>
