<?php
/**
 * Template Name: Salle de presse
 *
 */
?>

<?php get_header(); ?>

<div class="cols clearfix salle-de-presse">
	<h1 class="page-title"><?= get_the_title() ?></h1>

	<div class="content">
		<?= the_content() ?>
	</div>

	<div class="container">
		<div class="left-content">
			<?php
			get_press_releases();

			get_newsletters();
			?>
		</div>
		<div class="right-content">
			<h2 class="underlined"><?= __('Évènements récents', 'acem-salle-presse') ?></h2>
			<a href="<?= get_post_type_archive_link('evenement') ?>" class="all"><?= __('Voir tous les évènements', 'acem-salle-presse') ?> »</a>
			<?php
			get_recent_events();
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
