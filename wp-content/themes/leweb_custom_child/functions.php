<?php

/**** load Google fonts ****/
function load_fonts() {
    wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=Lato:300,400,100,100italic,300italic,700,700italic');
    wp_enqueue_style( 'googleFonts');
}
add_action('wp_print_styles', 'load_fonts');

/**** load scripts  ****/

function dp_adding_scripts() {
    wp_register_script('main', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'),'1.1', true);
    wp_enqueue_script('main');
}

add_action( 'wp_enqueue_scripts', 'dp_adding_scripts' );


function remove_media_queries() {
wp_dequeue_style( 'responsive-media-queries' );
}
add_action( 'wp_enqueue_scripts', 'remove_media_queries', 20 );


function is_page_child($pid) {// $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors( $post->ID );
    foreach($anc as $ancestor) {
        if(is_page() && $ancestor == $pid) {
            return true;
        }
    }
    if(is_page()&&(is_page($pid)))
        return true;   // we're at the page or at a sub page
    else
        return false;  // we're elsewhere
};

/**
 * Ajout du menu du footer
 */
function register_footer_menu() {
    register_nav_menu('footer-menu',__( 'Menu pied de page' ), 'acem-functions');
}
add_action( 'init', 'register_footer_menu' );


/**
 * Obtient les nouvelles préformattées pour affichage.
 *
 * La fonction obtient le contexte par la taxonome 'section' reliée à
 * la page actuelle via la globale $post.
 *
 * @return string
 */
function get_nouvelles() {
    global $post;
    $postid = $post->ID;

    $section = wp_get_post_terms($post->ID,'section');
    $sections = array();
    foreach ($section as $tax) {
        $sections[] = $tax->term_id;
    }
    $news = get_posts(array(
        'posts_per_page' => 5,
        'tax_query' => array(
            array(
                'taxonomy' => 'section',
                'terms' => $sections,
            )
        ),
        'suppress_filters' => 0
        )
    );

    ?>
    <div class="nouvelles col col-left section-border">
        <h1><?= __('Dernières nouvelles', 'acem-functions') ?></h1>
        <?php
        if (!empty($news)) {
            echo '<div class="items">';
            foreach ($news as $item) {
                $permalink = get_permalink($item);
                $excerpt = wp_html_excerpt($item->post_content, 300) . "...";
                ?>
                <div class="item">
                    <div>
                        <span class="date">[<?= date('Y-m-d',strtotime($item->post_date)) ?>]</span>
                        <h2><a href="<?= $permalink ?>"><?= $item->post_title ?></a></h2>
                    </div>
                    <div class="content"><?= $excerpt ?></div>
                    <div class="more"><a href="<?= $permalink ?>" class="jumplink"><?= __('Lire la suite', 'acem-functions') ?></a></div>
                </div>
                <?php
            }
            echo '</div>';
        } else {
            echo "<p class='center'>" . __('Aucune nouvelle à afficher', 'acem-functions') ."</p>";
        }
        ?>
    </div>
    <?php
}



/**
 * Obtient les documents préformattées pour affichage.
 *
 * La fonction obtient le contexte par la taxonome 'section' reliée à
 * la page actuelle via la globale $post.
 *
 * @return string
 */
function get_documents() {
    global $post;
    $postid = $post->ID;

    $section = wp_get_post_terms($post->ID,'section');
    $sections = array();
    foreach ($section as $tax) {
        $sections[] = $tax->term_id;
    }
    $docs = get_posts(array(
            'post_type' => 'document',
            'posts_per_page' => 5,
            'tax_query' => array(
                array(
                    'taxonomy' => 'section',
                    'terms' => $sections,
                )
            ),
            'suppress_filters' => 0
        )
    );

    ?>
    <div class="documents col col-right section-border">
        <h1><?= __('Derniers documents', 'acem-functions') ?></h1>
        <?php
        if (!empty($docs)) {
            echo '<div class="items">';
            foreach ($docs as $item) {
                $permalink = get_permalink($item);
                $excerpt = wp_html_excerpt($item->post_content, 300) . "...";
                $doctype = get_field('doctype', $item->ID);
                $fichier = get_field('file',$item->ID);
                $url = get_field('url',$item->ID);
                ?>
                <div class="item">
                    <div>
                        <span class="date">[<?= date('Y-m-d',strtotime($item->post_date)) ?>]</span>
                        <h2><a href="<?= $permalink ?>"><?= $item->post_title ?></a></h2>
                    </div>
                    <div class="content"><?= $excerpt ?></div>
                    <div class="more">
                        <?php if ($doctype == 'file') { ?>
                            <a href="<?= $fichier['url'] ?>" class="jumplink"><?= __('Télécharger le document', 'acem-functions') ?></a>
                        <?php } elseif ($doctype == 'url') { ?>
                            <a href="<?= $url ?>" class="jumplink" target="_blank"><?= __('Afficher le document', 'acem-functions') ?></a>
                        <?php } ?>
                        <a href="<?= $permalink ?>" class="jumplink"><?= __('Détails du document', 'acem-functions') ?></a>
                    </div>
                </div>
                <?php
            }
            echo '</div>';
        } else {
            echo "<p class='center'>" . __('Aucun document à afficher.', 'acem-functions') . "</p>";
        }
        ?>
    </div>
    <?php
}


/**
 * Retourne le slider ou rien s'il est vide
 *
 * Reprend le contexte de la page actuelle pour l'affichage
 */

function get_slider_partenaires() {
    global $post;
    $postid = $post->ID;

    $contenu = get_field('carrousel_partenaires',$postid);

    if (!empty($contenu)) {
        ?>
        <div class="contributeurs full-width section-border">
            <h1><?= __('Contributeurs', 'acem-functions') ?></h1>
            <div class="slider-contributeurs">
        <?php
        foreach ($contenu as $partner) {
            echo "<a href='{$partner['link']}' class='image'><img src='{$partner['image']['sizes']['slider-partenaires']}' alt=''></a>";
        }
        ?>
            </div>
        </div>
        <?php
    }
}


/**
 * Hook sur la fonction menu, ajoute item langue
 *
 * @param $items
 * @param $args
 * @return string
 */
function new_nav_menu_items($items,$args) {
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0');
        if(1 < count($languages)){
            foreach($languages as $l){
                if(!$l['active']){
                    $items = $items.'<li class="menu-item menu-item-custom-lang"><a href="'.$l['url'].'">'.$l['native_name'].'</a></li>';
                }
            }
        }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items',10,2 );


/**
 * Slider témoignages
 */
function get_slider_testimonials() {
    global $post;
    $postid = $post->ID;

    $section = wp_get_post_terms($postid,'section');
    $sections = array();
    foreach ($section as $tax) {
        $sections[] = $tax->term_id;
    }

    $testimonials = get_posts(array(
            'post_type' => 'temoignage',
            'posts_per_page' => 15,
            'orderby' => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'section',
                    'terms' => $sections,
                )
            ),
            'suppress_filters' => 0
        )
    );

    if (!empty($testimonials)) {
        echo '<div class="container-slider-testimonials">';
        echo '<div class="slider-testimonials">';
        foreach ($testimonials as $testimonial) {
            $name = get_field('name',$testimonial->ID);
            $quote = get_field('quote',$testimonial->ID);
            $image = get_field('image',$testimonial->ID);
            $link = get_field('link',$testimonial->ID);
            $imageUrl = $image['sizes']['slider-temoignages'];
            ?>
            <div class="testimonial">
                <img src="<?= $imageUrl ?>" alt="" class="image">
                <div class="matte-background"></div>

                <div class="infos">
                    <h2><?= $testimonial->post_title ?></h2>
                    <div class="name"><?= $name ?></div>
                    <div class="quote">
                        <?= $quote ?>
                    </div>
                    <?php if (!empty($link)) { ?>
                    <div class="link"><a href="<?= $link ?>" target="_blank"><i class="fa fa-globe"></i> <?= $testimonial->post_title ?></a></div>
                    <?php } ?>
                </div>
            </div>
            <?php
        }
        echo '</div>';
            echo '<div class="type-content">' . __('Témoignages', 'acem-functions') . '</div>';
        echo '</div>';
        ?>
        <script type="text/javascript">
            $ = jQuery;
            $(document).ready(function(){
                $('.slider-testimonials').slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    infinite: true,
                    arrows: false,
                    dots: true,
                    slidesToShow: 1
                });
            });
        </script>
        <?php
    } else {
        echo "<p class='center'>" . __('Aucun témoignage à afficher.', 'acem-functions') . "</p>";
    }
}


function get_all_news_and_events() {
    $contents = get_posts(array(
            'post_type' => array('post','evenement'),
            'posts_per_page' => 5,
            'suppress_filters' => 0
        )
    );

    if (!empty($contents)) {
        echo '<div class="all-news">';
        foreach ($contents as $content) {
            $permalink = get_permalink($content);
            if ($content->post_type == 'evenement') {
                $icone = '<i class="fa fa-calendar-o"></i>';
            } else {
                $icone = '<i class="fa fa-file-text-o"></i>';
            }
            ?>
            <div class="item">
                <h2>
                    <span class="icon"><?= $icone ?></span>
                    <span class="date">[<?= date('Y-m-d',strtotime($content->post_date)) ?>]</span>
                    <a href="<?= $permalink ?>"><?= $content->post_title ?></a>
                </h2>
            </div>
            <?php
        }
        echo '</div>';
    } else {
        echo "<p class='center'>" . __('Aucun document à afficher.', 'acem-functions') . "</p>";
    }
}


/**
 * Obtient les prochains évènements pour une catégorie donnée
 */

function get_section_events() {
    global $post;
    $postid = $post->ID;

    $section = wp_get_post_terms($postid,'section');
    $sections = array();
    foreach ($section as $tax) {
        $sections[] = $tax->term_id;
    }
    $events = get_posts(array(
            'post_type' => 'evenement',
            'posts_per_page' => 3,
            'orderby' => 'meta_value_num',
            'meta_key' => 'date',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'section',
                    'terms' => $sections,
                )
            ),
            'meta_query' => array(
                array(
                    'key' => 'date',
                    'value' => date('Ymd') - 1,
                    'compare' => '>='
                )
            ),
            'suppress_filters' => 0
        )
    );

    if (!empty($events)) {
        echo "<div class='section-events'>";
        foreach ($events as $event) {
            $id = $event->ID;

            $date = get_field('date',$id);
            $all_day = get_field('all_day',$id);
            $start_time = get_field('start_time',$id);
            $end_time = get_field('end_time',$id);
            $address_line_one = get_field('address_line_one',$id);
            $address_line_two = get_field('address_line_two',$id);

            if ($all_day) {
                $time = "{$date}, " . __('toute la journée', 'acem-functions');
            } elseif (!empty($start_time) and empty($end_time)) {
                $time = "{$date}, " . $start_time;
            } elseif (!empty($start_time) and !empty($end_time)) {
                $time = "{$date}, " . $start_time . ' - ' . $end_time;
            }

	        $permalink = get_permalink($id);
            ?>
            <div class="event">
                <h2><a href="<?= $permalink ?>"><?= $event->post_title ?></a></h2>

                <div class="date"><?= __('Date:', 'acem-functions') ?> <?= $time ?></div>
                <?php if (!empty($address_line_one)) {
                    $lineTwo = (!empty($address_line_two)) ? ', ' . $address_line_two : null;
                    ?>
                    <div class="location"><?= __('Lieu:', 'acem-functions') ?> <?= $address_line_one ?><?= $lineTwo ?></div>
                <?php } ?>
            </div>
            <?php
        }
        echo "</div>";
        echo "<a class=\"see-all\" href=\"" . get_post_type_archive_link('evenement') . "\">" . __('Voir tous les évènements', 'acem-functions') . " »</a>";
    } else {
        echo "<div class=''>". __('Aucun évènements à afficher.', 'acem-functions') ."</div>";
    }
}


/**
 * Modifie l'ordre par défaut pour le post type 'evenement'
 */

function modify_event_order( $query ) {

    if( is_admin() ) { return $query; }

    if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'evenement' && is_post_type_archive( 'evenement' )) {
        $query->set('orderby', 'meta_value_num');
        $query->set('meta_key', 'date');
        $query->set('order', 'ASC');
        $query->set('meta_query', array(
            array(
            'key' => 'date',
            'value' => date('Ymd') - 1,
            'compare' => '>='
            )
        ));
    }

    return $query;
}

add_action('pre_get_posts', 'modify_event_order');



/**
 * Retourne une table des matières pour du HTML
 */

function get_text_with_toc($html) {
	$doc = new DOMDocument();
	$doc->loadHTML(mb_convert_encoding('<div>'.PHP_EOL.$html.PHP_EOL.'</div>', 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);

	$xpath = new DOMXpath($doc);
	$headings = $xpath->query('//*/h2 | //*/h3 | //*/h4 | //*/h5 | //*/h6');

	if ($headings->length > 0) {
		echo "<div class='toc'><div class='title'>".__('Sections', 'acem-functions')."</div>";
		foreach ($headings as $heading) {
			$value = $heading->nodeValue;
			$id = strtolower(filter_var($value,FILTER_SANITIZE_URL));
			$heading->setAttribute('id',$id);
			echo "<a href='#{$id}' class='toc-item toc-{$heading->nodeName}'>{$value}</a>";
		}
		echo "</div>";
	}

	echo $doc->saveHTML();
}

function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
	$content = get_the_content($more_link_text, $stripteaser, $more_file);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}


/**
 * Sort une liste des derniers éléments de la revue de presse
 */

function get_press_releases()
{
	$pressReleases = get_posts(array(
			'post_type' => 'presse',
			'posts_per_page' => 3,
            'suppress_filters' => 0
		)
	);

	if (!empty($pressReleases)) {
		$all = get_post_type_archive_link('presse');
		echo "<div class='press-releases'>";
		echo "<h2 class='underlined'><a href='{$all}'>" . __('Revue de presse', 'acem-functions') . "</a></h2>";
		echo "<a href='{$all}' class='all'>" . __('Voir tous les articles', 'acem-functions') . " »</a>";

		foreach ($pressReleases as $item) {
			$id = $item->ID;
			$title = $item->post_title;
			$date = date('d/m/Y',strtotime($item->post_date));
			$excerpt = wp_html_excerpt(get_field('excerpt', $id), 120, "...");
			$type = get_field('type', $id);
			if ($type == 'document') {
				$typeMessage = __('Document téléchargable', 'acem-functions');
			} elseif ($type == 'link') {
				$typeMessage = __('Ressource Web', 'acem-functions');
			}
			$permalink = get_permalink($id);
			?>
			<div class="item">
				<a href="<?= $permalink ?>" class="title"><?= $title ?></a>
				<div class="infos"><?= $date ?> | <?= $typeMessage ?></div>
				<div class="excerpt"><?= $excerpt ?></div>
				<a href="<?= $permalink ?>" class="more"><?= __('Voir les détails', 'acem-functions') ?> »</a>
			</div>
			<?php
		}

		echo "</div>";
	}
}


/**
 * Sort les dernières infolettres des documents
 */

function get_newsletters()
{
	$newsletters = get_posts(array(
			'post_type' => 'document',
			'posts_per_page' => 3,
			'tax_query' => array(
				array(
					'taxonomy' => 'categorie-documentation',
					'terms' => array(22,30),
				)
			),
            'suppress_filters' => 0
		)
	);

	if (!empty($newsletters)) {
		$all = "/categorie-documentation/acemence/";
		echo "<div class='press-releases'>";
		echo "<h2 class='underlined'><a href='{$all}'>" . __("Infolettre L'ACEMENCE", 'acem-functions') . "</a></h2>";
		echo "<a href='{$all}' class='all'>" . __('Voir toutes les infolettres', 'acem-functions') . " »</a>";

		foreach ($newsletters as $item) {
			$id = $item->ID;
			$title = $item->post_title;
			$date = date('d/m/Y',strtotime($item->post_date));
			$excerpt = wp_html_excerpt($item->post_content, 120,"...");
			$permalink = get_permalink($id);
			?>
			<div class="item">
				<a href="<?= $permalink ?>" class="title"><?= $title ?></a>
				<div class="infos"><?= $date ?></div>
				<div class="excerpt"><?= $excerpt ?></div>
				<a href="<?= $permalink ?>" class="more"><?= __('Voir le document', 'acem-functions') ?> »</a>
			</div>
			<?php
		}

		echo "</div>";
	}
}


/**
 * Obtient les évènements récement terminés
 */

 function get_recent_events() {
	 $events = get_posts(array(
			 'post_type' => 'evenement',
			 'posts_per_page' => 5,
			 'orderby' => 'meta_value_num',
			 'meta_key' => 'date',
			 'order' => 'DESC',
			 'meta_query' => array(
				 array(
					 'key' => 'date',
					 'value' => date('Ymd') - 1,
					 'compare' => '<'
				 )
			 ),
             'suppress_filters' => 0
		 )
	 );

	 echo "<div class='recent-events'>";

	 foreach ($events as $event) {
		 $id = $event->ID;
		 $link_facebook = get_field('link_facebook',$id);
		 $content = wp_html_excerpt(get_field('description',$id), 300, "...");
		 $photo = get_field('photo',$id);

		 $date = get_field('date',$id);
		 $all_day = get_field('all_day',$id);
		 $start_time = get_field('start_time',$id);
		 $end_time = get_field('end_time',$id);
		 $address_line_one = get_field('address_line_one',$id);
		 $address_line_two = get_field('address_line_two',$id);

		 if ($all_day) {
			 $time = "{$date}, " . __('toute la journée', 'acem-functions');
		 } elseif (!empty($start_time) and empty($end_time)) {
			 $time = "{$date}, " . $start_time;
		 } elseif (!empty($start_time) and !empty($end_time)) {
			 $time = "{$date}, " . $start_time . ' - ' . $end_time;
		 }
		 ?>
		 <div class="event">
			 <?php
			 if(!empty($photo)) {
			    $photoUrl = $photo['sizes']['thumb-large'];
			    ?>
			    <img src="<?= $photoUrl ?>" alt="" class="photo">
			 <?php } ?>
			 <div class="infos">
				 <div class="title"><?= $event->post_title ?></div>
				 <div class="date"><?= __('A eu lieu le', 'acem-functions') ?> <?= $time ?></div>
				 <div class="content"><?= $content ?></div>
				 <?php
				 if (!empty($link_facebook)) {
                    $message = get_field('link_title',$id);
					?>
					<a href="<?= $link_facebook ?>" target="_blank"><?= $message ?> »</a>
					<?php
				 }
				 ?>
			 </div>
		 </div>
		<?php
	 }

	 echo "</div>";
 }


/**
 * Get tags
 */

function get_section_tags() {
	global $post;
	$postid = $post->ID;

	$sections_terms = wp_get_post_terms($postid, 'section');
	if (!empty($sections_terms)) {
		echo "<div class='section-tags'>";
		echo '<i class="fa fa-tags"></i>';
		foreach ($sections_terms as $tax) {
			$sections[] = $tax->term_id;
			$color = get_field('section_color',$tax);
            $page = get_field('page_section',$tax);
			echo "<a href='$page' class='tag' style='background-color:{$color}'>{$tax->name}</a>";
		}
		echo "</div>";
	}
}

/**
 * Get document tags, translated
 */

function get_localized_post_type() {
    global $post;
    $postid = $post->ID;

    $postType = get_post_type($postid);

    if ($postType == 'post') {
        $out = __('Article','acem-functions');
    } elseif ($postType == 'page') {
        $out = __('Page','acem-functions');
    } elseif ($postType == 'document') {
        $out = __('Document','acem-functions');
    } elseif ($postType == 'temoignage') {
        $out = __('Témoignage','acem-functions');
    } elseif ($postType == 'evenement') {
        $out = __('Évènement','acem-functions');
    } elseif ($postType == 'presse') {
        $out = __('Presse','acem-functions');
    } else {
        $out = null;
    }

    if ($out !== null) {
        echo '<span class="loc-post-type">[' . $out . ']</span>';
    }
}

/**
 * Get press room url for titles
 */
 function get_press_room_url() {
     if (ICL_LANGUAGE_CODE == 'en') {
         $postId = 664;
     } else {
         $postId = 590;
     }

     $post = get_post($postId);
     $title = $post->post_title;
     $permalink = get_permalink($post->ID);

     return "<a href='$permalink'>{$title}</a> » ";
 }
