<?php
/**
 * Template Name: Accueil
 *
 */
?>

<?php
get_header();

?>
<div class="home-content cols clearfix">
	<div class="home-slider">
		<?php get_slider_testimonials() ?>
	</div>

	<div class="clienteles">
		<?php


		$clienteles = array();
		if (ICL_LANGUAGE_CODE == 'fr') {
			$clienteles['financement'] = get_post(431);
			$clienteles['formation'] = get_post(433);
			$clienteles['acquis'] = get_post(435);
			$clienteles['investir'] = get_post(437);
		} else {
			$clienteles['financement'] = get_post(620);
			$clienteles['formation'] = get_post(622);
			$clienteles['acquis'] = get_post(634);
			$clienteles['investir'] = get_post(630);
		}

		foreach ($clienteles as $name => $data) {
			$id = $data->ID;
			$clientele = get_field('clientele',$id);
			$icon = get_field('clientele_icon',$id);
			$goal = get_field('clientele_goal',$id);
			$color = get_field('section_color',$id);
			?>
			<a href="<?= get_permalink($data); ?>" class="clientele" style="background-color:<?= $color ?>">
				<div class="name"><?= $clientele ?></div>
				<div class="logo"><img src="<?= $icon['url'] ?>"></div>
				<div class="goal"><?= $goal ?></div>
			</a>
			<?php
		}

		?>
	</div>

	<div class="bottom-info">
		<div class="welcome">
			<?= get_the_content() ?>
		</div>

		<div class="actualites">
			<h1><?= __('Nos actualités et évènements', 'acem-home') ?></h1>
			<?php get_all_news_and_events() ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
