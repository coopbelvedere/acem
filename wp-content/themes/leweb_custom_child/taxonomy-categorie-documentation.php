<?php
/** Template pour l'affichage des catégories de documents */
?>

<?php get_header();

$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$docuCenterLink = __('/centre-de-documentation/', 'acem-taxo-cat-documentation');
?>
<div class="cols clearfix centre-documentation">
	<h1 class="page-title"><a href="<?= $docuCenterLink ?>">Documents</a> » <?= $current_term->name ?></h1>

	<div class="documents no-col">
	<?php
	if (have_posts()) {
		echo "<div class='items'>";

		while (have_posts()) {
			the_post();
			$id = get_the_ID();
			$file = get_field('file',$id);
			$ext = mb_strtoupper(pathinfo($file['filename'], PATHINFO_EXTENSION));
			$extString = sprintf(__('Document %s'), $ext, 'acem-taxo-cat-documentation');
			$permalink = get_permalink($id);
			$description = wp_html_excerpt(get_the_content(), 60) . '...';
			?>
			<div class="item">
				<a href="<?= $permalink ?>" class="title"><?= get_the_title(); ?></a>
				<div class="ext"><?= get_the_date('Y-m-d') ?> &mdash; <?= $extString ?></div>
				<div class="description"><?= $description ?></div>
				<a href="<?= $permalink ?>" class="link"><?= __('Voir le document', 'acem-taxo-cat-documentation') ?> »</a>
			</div>
			<?php
		}

		echo "</div>";
	}
	?>
	</div>
</div>
<?php get_footer(); ?>
