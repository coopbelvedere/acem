<?php get_header();

?>
<div class="cols clearfix post-single">
	<h1 class="page-title"><?= the_title() ?></h1>

	<div class="document no-col">
		<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();

				?>
				<div class="publish-date">
					<?= __('Publié le', 'acem-single') ?> :
					<?= get_the_date('Y-m-d') ?>
				</div>

				<?php get_section_tags(); ?>

				<div class="text"><?php the_content() ?></div>

				<div class="next-prev">
					<div class="title"><?= __("Lire d'autres articles", 'acem-single') ?></div>
					<div class="both">
						<div class="prev">
							<?php previous_post_link();	?>
						</div>
						<div class="next">
							<?php next_post_link(); ?>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>
<?php get_footer(); ?>
