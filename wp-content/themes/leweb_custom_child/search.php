<?php get_header(); ?>

<section class="content">

	<?php get_template_part('inc/page-title'); ?>
	
	<div class="pad group">
		
		<div class="notebox">
			<?php _e('Pour le terme','acem-search'); ?> "<span><?php echo get_search_query(); ?></span>".
			<?php if ( !have_posts() ): ?>
				<?php _e('Veuillez tenter une autre recherche :','acem-search'); ?>
			<?php endif; ?>
			<div class="search-again">
				<?php get_search_form(); ?>
			</div>
		</div>
		
		<?php if ( have_posts() ) : ?>
		
			<div class="post-list group search-results">
				<?php
				$i = 1; echo '<div class="post-row">'; while ( have_posts() ) {
					the_post();
					?>
					<div class="result">
						<h2><?php get_localized_post_type() ?> <a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
						<?php get_section_tags() ?>
					</div>
					<?php
				}
				echo '</div>'; ?>
			</div><!--/.post-list-->
		
			<?php get_template_part('inc/pagination'); ?>
			
		<?php endif; ?>
		
	</div><!--/.pad-->
	
</section><!--/.content-->

<?php get_sidebar(); ?>
	
<?php get_footer(); ?>