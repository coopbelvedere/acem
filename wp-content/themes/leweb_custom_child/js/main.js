jQuery(document).ready( function() {
    var $ = jQuery;

    function hideMenus() {
        menuTimer = window.setTimeout(function() {
            $("#sub-menu-box").css({ "background-color": "#000" });
            $("#menu-second > li > a, #menu-second-anglais0 >li > a").removeClass("hover");
            $(".sub-menu").css({"display": "none"})
            // Except current one
            $("#menu-second > li.current_page_ancestor, #menu-second-anglais0 > li.current_page_ancestor").find(".sub-menu").css({ "display": "block", "visibility": "visible" });
            $("#menu-second > li.current_page_ancestor > a, #menu-second-anglais0 > li.current_page_ancestor > a").addClass("hover");
            $("#sub-menu-box").css({ "background-color": $("#menu-second > li.current_page_ancestor > a, #menu-second-anglais0 > li.current_page_ancestor > a").css("backgroundColor") });
        }, 750);
    }

    $(".sub-menu").each( function() {
        $(this).css({ "display": "block", "visibility": "hidden" });
        $(this).css({ "left": $("#wrapper").offset().left  - $(this).offset().left +"px"});
        $(this).css({ "display": "none", "visibility": "visible" });
        $(this).closest(".current_page_ancestor").find(".sub-menu").css({ "display": "block", "visibility": "visible" });
        $("#menu-second > li.current_page_ancestor > a, #menu-second-anglais0 > li.current_page_ancestor > a").addClass("hover");
        $("#sub-menu-box").css({ "background-color": $("#menu-second > li.current_page_ancestor > a, #menu-second-anglais0 > li.current_page_ancestor > a").css("backgroundColor") });
    });

    $("#menu-second > li > a, #menu-second-anglais0 > li > a").mouseover( function() {
        $(".sub-menu").css({"display": "none"});
        $("#sub-menu-box").css({ "background-color": $(this).css("backgroundColor") });
        $("#menu-second a, #menu-second-anglais0 a").removeClass("hover");
        $(this).addClass("hover");
        $(this).parent().find(".sub-menu").css({"display": "block"});

        if(typeof menuTimer !== 'undefined') {
            window.clearTimeout(menuTimer);
        }
    });

    $("#sub-menu-box, .sub-menu").mouseover(function() {
        if(typeof menuTimer !== 'undefined') {
            window.clearTimeout(menuTimer);
        }
    });

    $("#sub-menu-box, .sub-menu").mouseleave(function() {
        hideMenus();
    });

    $("#menu-second > li, #menu-second-anglais0 > li").mouseleave( function(menuTimer) {
        hideMenus();
    });
});