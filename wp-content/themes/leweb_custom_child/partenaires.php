<?php
/**
 * Template Name: Partenaires
 *
 */
?>

<?php get_header(); ?>
<div class="cols clearfix partenaires">
	<h1 class="page-title"><?= get_the_title() ?></h1>

	<div class="content">
		<?= the_content() ?>
	</div>

	<div class="liste-partenaires">
		<?php
		$partners = get_field('partners');

		foreach ($partners as $partner) {
			$url = $partner["link"];
			$link = (!empty($url)) ? "<div class='link'><a href='{$partner['link']}' target='_blank'>" . __('Site du partenaire', 'acem-partenaires') . " »</a></div>" : null;
			?>
			<div class="partenaire clearfix">
				<div class="logo">
					<img src="<?= $partner['logo']['sizes']['medium'] ?>" alt="">
				</div>
				<div class="infos">
					<div class="title"><?= $partner['name'] ?></div>
					<div class="description"><?= $partner['description'] ?></div>
					<?= $link ?>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>
<?php get_footer(); ?>
