<?php

?>

<?php get_header(); ?>
<div class="cols clearfix archive-evenements">
	<h1 class="page-title"><?= __('Évènements', 'acem-archive-evenement') ?></h1>

	<?php
	if (have_posts()) {
		echo "<div class='evenements'>";
		while (have_posts()) {
			the_post();
			$id = get_the_ID();

			$description = get_field('description',$id);
			$date = get_field('date',$id);
			$all_day = get_field('all_day',$id);
			$start_time = get_field('start_time',$id);
			$end_time = get_field('end_time',$id);
			$email = get_field('email',$id);
			$reservation = get_field('reservation',$id);
			$address_line_one = get_field('address_line_one',$id);
			$address_line_two = get_field('address_line_two',$id);
			$image = get_field('image',$id);
			if (isset($image)) {
				$image = $image['sizes']['thumb-large'];
			}

			if ($all_day) {
				$time = "{$date}, " . __('toute la journée', 'acem-archive-evenement');
			} elseif (!empty($start_time) and empty($end_time)) {
				$time = "{$date}, " . $start_time;
			} elseif (!empty($start_time) and !empty($end_time)) {
				$time = "{$date}, " . $start_time . ' - ' . $end_time;
			}

			$excerpt = wp_html_excerpt($description, 350);

			?>

			<div class="evenement clearfix">
				<img src="<?= $image ?>" class="coverimage" alt="">

				<div class="infos">
					<h2><a href="<?= the_permalink() ?>"><?= get_the_title() ?></a></h2>
					<div class="date"><?= __('Date:', 'acem-archive-evenement') ?> <?= $time ?></div>
					<?php get_section_tags() ?>
					<?php if (!empty($address_line_one)) {
						$lineTwo = (!empty($address_line_two)) ? ', ' . $address_line_two : null;
						?>
						<div class="location"><?= __('Lieu:', 'acem-archive-evenement') ?> <?= $address_line_one ?><?= $lineTwo ?></div>
					<?php }

					if (!empty($excerpt)) {
						?>
						<div class="excerpt">
							<?= $excerpt . "..." ?>
						</div>
						<?php
					}
					?>

					<div class="more">
						<a href="<?= the_permalink() ?>"><?= __("Afficher l'évènement", 'acem-archive-evenement') ?> »</a>
					</div>
				</div>
			</div>

			<?php
		}
		echo "</div>";
	}
	?>
</div>
<?php get_footer(); ?>
